#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <WiFiUdp.h>
#include <Wire.h>
#include <WebSocketsClient.h>
#include <Hash.h>
#include <Arduino.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_LSM303_U.h>
const char* ssid = "LKCS-CKAR";
const char* password = "projectlkcs";
int intheading;
int x = 0;
bool checklr = false;
bool checkfor = false;
//internal variables
int steps = 500;
int turnsteps = 500;
int currentheading = 180;
int targetheading;
void senddata(String);
int getheading();
ESP8266WiFiMulti WiFiMulti;
WebSocketsClient webSocket;
String const Mobotname = "Mobot-3";
Adafruit_LSM303_Mag_Unified mag = Adafruit_LSM303_Mag_Unified(12345); //magnetometer i2cid


void webSocketEvent(WStype_t type, uint8_t * payload, size_t length) {

  switch (type) {
    case WStype_DISCONNECTED:
      //Serial.println("[" + Mobotname + "] Disconnected!\n");
      break;
    case WStype_CONNECTED: {
        //Serial.println("[" + Mobotname + "]" + "Connected to Server\n");

        // send message to server when Connected
        webSocket.sendTXT("[" + Mobotname + "]" + " Succesfully Connected");
      }
      break;
    case WStype_TEXT://recieve data here
      int transnum;
      Serial.printf("[Mobot1] get text: %s\n", payload);
      if (strncmp((const char*)payload, "forward", 6) == 0) {
        senddata("i received forward CurrH: " + String(intheading));
        transnum = 400;
        Wire.beginTransmission(8); // transmit to device #8
        Wire.write(lowByte(transnum));
        Wire.write(highByte(transnum));
        Wire.endTransmission();    // stop transmitting
        checkfor = true;
        break;
      } else if (strncmp((const char*)payload, "left", 4) == 0) {
        senddata("i received LEFT");
        transnum = 500;
        Wire.beginTransmission(8); // transmit to device #8
        Wire.write(lowByte(transnum));
        Wire.write(highByte(transnum));        // sends five bytes
        Wire.endTransmission();    // stop transmitting
        checklr = true;
        break;
      } else if (strncmp((const char*)payload, "right", 5) == 0) {
        senddata("i received RIGHT");
        transnum = 600;
        Wire.beginTransmission(8); // transmit to device #8
        Wire.write(lowByte(transnum));
        Wire.write(highByte(transnum));
        Wire.endTransmission();
        checklr = true;
        break;
      }
      break;
  }

}

void setup() {
  Serial.begin(9600);           // start serial for output
  if (!mag.begin())
  {

  }
  Wire.begin(7);

  for (uint8_t t = 4; t > 0; t--) {
    delay(1000);
  }

  WiFiMulti.addAP(ssid, password);

  //WiFi.disconnect();
  while (WiFiMulti.run() != WL_CONNECTED) {
    delay(1000);
  }

  // server address, port and URL
  webSocket.begin("10.0.0.192", 3000, "/");

  // event handler
  webSocket.onEvent(webSocketEvent);


  // try ever 5000 again if connection has failed
  webSocket.setReconnectInterval(5000);
}

void loop() {
  if (checklr == true) {
    delay(2000);
    senddata("DONE");
    checklr = false;
  }
  if (checkfor == true) {
    delay(6000);
    senddata("DONE");
    checkfor = false;
  }
  getheading();
}

int getheading()
{
  sensors_event_t event;
  mag.getEvent(&event);

  float Pi = 3.14159;

  // Calculate the angle of the vector y,x
  float heading = (atan2(event.magnetic.y, event.magnetic.x) * 180) / Pi;

  // Normalize to 0-360
  if (heading < 0)
  {
    heading = 360 + heading;
  }
  intheading = heading;
  Wire.beginTransmission(8); // transmit to device #8
  Wire.write(lowByte(intheading));
  Wire.write(highByte(intheading));
  Wire.endTransmission();    // stop transmitting
  delay(10);
  return intheading;
}
void senddata(String message)
{
  webSocket.sendTXT("[" + Mobotname + "] " + message);
}
