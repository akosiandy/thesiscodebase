var express = require('express');
var app = express();
var url = require('url');
var http = require('http').Server(app);
var WebSocketServer = require('ws').Server;
var wss = new WebSocketServer({
	server: http
});



let mobotmap = [
	[1, 0, 0, 0],
	[2, 0, 0, 0],
	[3, 0, 0, 0],
	[4, 0, 0, 0]
];
let grid = [
	[1, 5, 9, 13],
	[2, 6, 10, 14],
	[3, 7, 11, 15],
	[4, 8, 12, 16]
]
// n6-e7-s8-w9
let heading = [
	[1, 6],
	[2, 6],
	[3, 6],
	[4, 6]
];
var m1buff = [];
var m2buff = [];
var m3buff = [];
var m4buff = [];
var mobot = {};
var mbotto1;
var mbotto2;
var mbotto3;
var mbotto4;
var id = -1;
var wsglobal;
var interf = {};
wss.on('connection', function connection(ws) {
	wsglobal = ws;
	id++;
	ws.id = "Mobot-" + id;
	mobot[ws.id] = ws;


	console.log('New Device Connected' + "Mobot-" + id);
	ws.on('message', function incoming(message) {
		console.log(message);
		if (message.substr(0, 2) == 88) {
			var mbot = message.substr(2, 1);
			var mbottoloc = message.substr(3, 2);
			console.log(mbot);
			console.log(mbottoloc);
			generatemovement(ws, mbot, mbottoloc);
		}

		if (message === "forward" || message === "left" || message === "right") {
			wss.clients.forEach(function each(client) { //send message to all connected devices
				//console.log('[BROADCAST]');
				client.send(message);
				console.log("B:" + message)
			});
		}
		//command interface command cmdm112 cmd -m1 - 12
		if (message.substr(0, 3) === "cmd") {
			//mobot to location 
			var mbot = message.substr(4, 2);
			var mbotto = message.substr(7, 2);
			generatemovement(ws, mbot, mbotto);

		} else {
			var mname = message.substr(1, 7)
			//systeminformer for left forward and right turn as done
			var str = message;
			var ackl = str.substring(10);
			if (ackl == "DONE") {
				//check for buffer first
				sendinterface(ws, mobotmap);
				console.log(mobotmap);
				if (processbuffer(ws, mname)) {
					console.log("checkingbuffer");
					console.log(m1buff);
				} else {
					var mnametombotconv = mname.substr(0, 1) + mname.substr(6, 1);
					generatemovement(ws, mnametombotconv.toLowerCase(), -1);
				}
			}
		}


		wss.clients.forEach(function each(client) { //send message to all connected devices
			//console.log('[BROADCAST]');
			//client.send(message);
			//console.log("B:"+ message)
		});

		wss.on('close', function (ws) {
			delete mobot[ws.id];
			console.log('deleted: ' + userID)
		});

	});
});

app.get('/', function (req, res) {
	res.sendFile(__dirname + '/index.html');
});
app.use(express.static('public'));
wss.on('error', function (error) {
	console.log(error);
});

http.listen(3000, function () {
	console.log('listening on *:3000');
});


function generatemovement(ws, mbot, mbotto) {
	var findme;
	console.log(mbot);
	if (mbot === "m1" || mbot == 1) {
		findme = 1;
		if (mbotto == -1) {
			mbotto = mbotto1;
		} else {
			mbotto1 = mbotto;
		}
	} else if (mbot === "m2" || mbot == 2) {
		findme = 2;
		if (mbotto === -1) {
			mbotto = mbotto2;
		} else {
			mbotto2 = mbotto;
		}
	} else if (mbot === "m3" || mbot == 3) {
		findme = 3;
		if (mbotto === -1) {
			mbotto = mbotto3;
		} else {
			mbotto3 = mbotto;
		}
	} else if (mbot === "m4" || mbot == 4) {
		findme = 4;
		if (mbotto === -1) {
			mbotto = mbotto4;
		} else {
			mbotto4 = mbotto;
		}
	}
	if (mbotto === undefined || mbotto === -1) {
		return 0;
	} else {
		var xy = findgrid(findme)
		createtrack(ws, findme, xy[0], xy[1], mbotto)
	}
}
//process the grid 
function createtrack(ws, findme, x, y, mbotto) {
	console.log(findme + "Going to " + mbotto);
	console.log(findme + "CurLocation" + grid[x][y]);
	var getorient = heading[findme - 1][1];
	var gridvaluecurloc = grid[x][y];
	var togridloc = findgridmapvalue(mbotto);
	//check mobot location
	// check for coordinates if can process plus one check for x or y 

	//left to right only 
	if (gridvaluecurloc <= mbotto) {
		if (x === togridloc[0] && y === togridloc[1]) {

		} else {
			if (y !== togridloc[1]) {
				if (mobotmap[x][y + 1] === 0) { // check if forward is possble forward is priority
					ltor(ws, findme, getorient, x, y);
				} else if (mobotmap[x + 1][y] === 0) {
					toptobot(ws, findme, getorient, x, y);
				}
				else if (mobotmap[x - 1][y] === 0) {
					bottotop(ws, findme, getorient, x, y);
				} else if (mobotmap[x][y - 1] === 0) {
					rtol(ws, findme, getorient, x, y);
				}
			} else if (x !== togridloc[0]) {
				if (mobotmap[x + 1][y] === 0) {
					toptobot(ws, findme, getorient, x, y);
				} else if (mobotmap[x][y + 1] === 0) { // check if forward is possble forward is priority
					ltor(ws, findme, getorient, x, y);
				} else if (mobotmap[x - 1][y] === 0) {
					bottotop(ws, findme, getorient, x, y);
				}
			}
		}
	} else if (gridvaluecurloc >= mbotto) {
		if (y !== togridloc[1]) {
			if (mobotmap[x][y - 1] === 0) {
				rtol(ws, findme, getorient, x, y);
			} else if (mobotmap[x - 1][y] === 0) {
				bottotop(ws, findme, getorient, x, y);
			}
			else if (mobotmap[x + 1][y] === 0) {
				toptobot(ws, findme, getorient, x, y);
			}
		} else if (x !== togridloc[0]) {
			if (mobotmap[x - 1][y] === 0) {
				bottotop(ws, findme, getorient, x, y);
			} else if (mobotmap[x][y - 1] === 0) {
				rtol(ws, findme, getorient, x, y);
			} else if (mobotmap[x + 1][y] === 0) {
				toptobot(ws, findme, getorient, x, y);
			}
		}
	}
}

function rtol(ws, findme, getorient, x, y) {
	if (getorient === 6) {
		addtobuffer(findme, "forward");
		pushmovement(ws, findme, "left");
		heading[findme - 1][1] = 9;
	} else if (getorient === 7) {
		addtobuffer(findme, "forward");
		addtobuffer(findme, "right");
		pushmovement(ws, findme, "right");
		heading[findme - 1][1] = 9;
	} else if (getorient === 8) {
		addtobuffer(findme, "forward");
		pushmovement(ws, findme, "right");
		heading[findme - 1][1] = 9;
	} else if (getorient === 9) {
		pushmovement(ws, findme, "forward");
		mobotmap[x][y + 1] === findme;
		processmobotmap(findme, "forward");
	}
}

function bottotop(ws, findme, getorient, x, y) {
	if (getorient === 7) {
		addtobuffer(findme, "forward");
		pushmovement(ws, findme, "left");
		heading[findme - 1][1] = 7;
	} else if (getorient === 8) {
		addtobuffer(findme, "left");
		addtobuffer(findme, "forward");
		pushmovement(ws, findme, "left");
		heading[findme - 1][1] = 7;
	} else if (getorient === 9) {
		addtobuffer(findme, "forward");
		pushmovement(ws, findme, "right");
		heading[findme - 1][1] = 6;
	} else {
		pushmovement(ws, findme, "forward");
		mobotmap[x + 1][y] === findme;
		processmobotmap(findme, "forward");
	}
}

function toptobot(ws, findme, getorient, x, y) {
	if (getorient === 6) {
		addtobuffer(findme, "right");
		addtobuffer(findme, "forward");
		pushmovement(ws, findme, "right");
		heading[findme - 1][1] = 7;
	} else if (getorient === 7) {
		addtobuffer(findme, "forward");
		pushmovement(ws, findme, "right");
		heading[findme - 1][1] = 8;
	} else if (getorient === 9) {
		addtobuffer(findme, "forward");
		pushmovement(ws, findme, "left");
		heading[findme - 1][1] = 8;
	} else {
		pushmovement(ws, findme, "forward");
		mobotmap[x + 1][y] === findme;
		processmobotmap(findme, "forward");
	}
}

function ltor(ws, findme, getorient, x, y) {
	if (getorient === 6) {
		addtobuffer(findme, "forward");
		pushmovement(ws, findme, "right");
		heading[findme - 1][1] = 7;
	} else if (getorient === 8) {
		addtobuffer(findme, "forward");
		pushmovement(ws, findme, "left");
		heading[findme - 1][1] = 7;
	} else if (getorient === 9) {
		addtobuffer(findme, "forward");
		addtobuffer(findme, "left");
		pushmovement(ws, findme, "left");
		heading[findme - 1][1] = 7;
	} else {
		pushmovement(ws, findme, "forward");
		mobotmap[x][y + 1] === findme;
		processmobotmap(findme, "forward");
	}
}

function findgrid(find) {
	for (x = 0; x <= 3; x++) {
		for (y = 0; y <= 3; y++) {
			if (mobotmap[x][y] === find) {
				return [x, y];
			}
		}
	}

}

function findgridmapvalue(find) {
	for (x = 0; x <= 3; x++) {
		for (y = 0; y <= 3; y++) {
			if (grid[x][y] == find) {
				return [x, y];
			}
		}
	}

}

function addtobuffer(mbot, message) {
	if (mbot === 1) {
		m1buff.push(message);
	} else if (mbot === 2) {
		m2buff.push(message);
	} else if (mbot === 3) {
		m3buff.push(message);
	} else if (mbot === 4) {
		m4buff.push(message);
	}

}

function pushmovement(ws, mbot, message) {
	if (mbot === 1) {
		mobot["Mobot-1"].send(message);

	} else if (mbot === 2) {
		mobot["Mobot-2"].send(message);

	} else if (mbot === 3) {
		mobot["Mobot-3"].send(message);

	} else if (mbot === 4) {
		mobot["Mobot-4"].send(message);

	}



}
// process the location of the mobot 
function processmobotmap(mvar, buff) {
	if (buff === "forward") {
		var curlocgrid = findgrid(mvar);
		var header = heading[mvar - 1][1];
		if (header === 6) {
			mobotmap[curlocgrid[0]][curlocgrid[1]] = 0;
			mobotmap[curlocgrid[0] - 1][curlocgrid[1]] = mvar;
		}
		if (header === 7) {
			mobotmap[curlocgrid[0]][curlocgrid[1]] = 0;
			mobotmap[curlocgrid[0]][curlocgrid[1] + 1] = mvar;
		}
		if (header === 8) {
			mobotmap[curlocgrid[0]][curlocgrid[1]] = 0;
			mobotmap[curlocgrid[0] + 1][curlocgrid[1]] = mvar;
		}
		if (header === 9) {
			mobotmap[curlocgrid[0]][curlocgrid[1]] = 0;
			mobotmap[curlocgrid[0]][curlocgrid[1] - 1] = mvar;
		}
	}

}

var buff;
// check if there are existing commands before contactting the traffic control
function processbuffer(ws, mname) {
	if (mname === "Mobot-1") {
		if (m1buff.length === 0) { } else {
			buff = m1buff.pop();
			mobot[mname].send(buff);
			processmobotmap(1, buff);
			return true;
		}
	} else if (mname === "Mobot-2") {
		if (m2buff.length === 0) { } else {
			buff = m2buff.pop();
			mobot[mname].send(buff);
			processmobotmap(2, buff);
			return true;
		}
	} else if (mname === "Mobot-3") {
		if (m3buff.length === 0) { } else {
			buff = m3buff.pop();
			mobot[mname].send(buff);
			processmobotmap(3, buff);
			return true;
		}
	} else if (mname === "Mobot-4") {
		if (m4buff.length === 0) { } else {
			buff = m4buff.pop();
			mobot[mname].send(buff);
			processmobotmap(4, buff);
			return true;
		}
	}
	processheader(mname, buff);
	console.log("exited");
	return false;
}

function processheader(mname, buff) {
	var mv;
	if (buff === "forward") {
		return 0;
	} else {

		if (mname === "Mobot-1") {
			mv = 1;
		} else if (mname === "Mobot-2") {
			mv = 2;
		} else if (mname === "Mobot-3") {
			mv = 3;
		} else if (mname === "Mobot-4") {
			mv = 4;
		}

		if (buff === "left") {
			if (heading[mv][1] === 6) {
				heading[mv][1] = 9;
			} else {
				heading[mv][1] = heading[mv][1] - 1;
			}
		} else if (buff == "right") {
			if (heading[mv][1] === 9) {
				heading[mv][1] = 6;
			} else {
				heading[1] = heading[mv][1] - 1;
			}
		}
	}
}

function sendinterface(ws, mobotmap) {

	console.log("sendinggrid");
	mobot["Mobot-0"].send(JSON.stringify(mobotmap));
}