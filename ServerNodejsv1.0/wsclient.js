var webSocket = null;
var ws_protocol = null;
var ws_hostname = null;
var ws_port = null;
var ws_endpoint = null;


/**
 * Event handler for clicking on button "Connect"
 */
function onConnectClick() {
    var ws_protocol = 'ws';
    var ws_hostname = '10.0.0.192';
    var ws_port = 3000;
    var ws_endpoint = '';
    openWSConnection(ws_protocol, ws_hostname, ws_port, ws_endpoint);
}
/**
 * Event handler for clicking on button "Disconnect"
 */
function onDisconnectClick() {
    webSocket.close();
}
/**
 * Open a new WebSocket connection using the given parameters
 */
function openWSConnection(protocol, hostname, port, endpoint) {
    var webSocketURL = null;
    webSocketURL = protocol + "://" + hostname + ":" + port + endpoint;
    console.log("openWSConnection::Connecting to: " + webSocketURL);
    try {
        webSocket = new WebSocket(webSocketURL);
        webSocket.onopen = function (openEvent) {
            console.log("WebSocket OPEN: " + JSON.stringify(openEvent, null, 4));
            document.getElementById("btnSend").disabled = false;
            document.getElementById("btnConnect").disabled = true;
            document.getElementById("btnDisconnect").disabled = false;
            document.getElementById("grid01").style.backgroundColor = "white";
            document.getElementById("grid02").style.backgroundColor = "white";
            document.getElementById("grid03").style.backgroundColor = "white";
            document.getElementById("grid11").style.backgroundColor = "white";
            document.getElementById("grid12").style.backgroundColor = "white";
            document.getElementById("grid13").style.backgroundColor = "white";
            document.getElementById("grid21").style.backgroundColor = "white";
            document.getElementById("grid22").style.backgroundColor = "white";
            document.getElementById("grid23").style.backgroundColor = "white";
            document.getElementById("grid31").style.backgroundColor = "white";
            document.getElementById("grid32").style.backgroundColor = "white";
            document.getElementById("grid33").style.backgroundColor = "white";
            document.getElementById("grid01").textContent = "";
            document.getElementById("grid02").textContent = "";
            document.getElementById("grid03").textContent = "";
            document.getElementById("grid11").textContent = "";
            document.getElementById("grid12").textContent = "";
            document.getElementById("grid13").textContent = "";
            document.getElementById("grid21").textContent = "";
            document.getElementById("grid22").textContent = "";
            document.getElementById("grid23").textContent = "";
            document.getElementById("grid31").textContent = "";
            document.getElementById("grid32").textContent = "";
            document.getElementById("grid33").textContent = "";
            webSocket.send("interface connected");
        };
        webSocket.onclose = function (closeEvent) {
            console.log("WebSocket CLOSE: " + JSON.stringify(closeEvent, null, 4));
            document.getElementById("btnSend").disabled = true;
            document.getElementById("btnConnect").disabled = false;
            document.getElementById("btnDisconnect").disabled = true;
        };
        webSocket.onerror = function (errorEvent) {
            console.log("WebSocket ERROR: " + JSON.stringify(errorEvent, null, 4));
        };
        webSocket.onmessage = function (messageEvent) {
            var wsMsg = messageEvent.data;
            console.log("WebSocket MESSAGE: " + wsMsg);
            clearinterface();
            if (wsMsg.substr(2, 1) != 0) {
                document.getElementById("grid00").style.backgroundColor = "darkcyan";
                document.getElementById("grid00").textContent = "Mobot - " + wsMsg.substr(2, 1);
            }
            if (wsMsg.substr(4, 1) != 0) {
                document.getElementById("grid01").style.backgroundColor = "darkcyan";
                document.getElementById("grid01").textContent = "Mobot - " + wsMsg.substr(4, 1);
            }
            if (wsMsg.substr(6, 1) != 0) {
            
                document.getElementById("grid02").style.backgroundColor = "darkcyan";
                document.getElementById("grid02").textContent = "Mobot - " + wsMsg.substr(6, 1);
            }
            if (wsMsg.substr(8, 1) != 0) {
                
                document.getElementById("grid03").style.backgroundColor = "darkcyan";
                document.getElementById("grid03").textContent = "Mobot - " + wsMsg.substr(8, 1);
            }

            if (wsMsg.substr(12, 1) != 0) {
                
                document.getElementById("grid10").style.backgroundColor = "darkcyan";
                document.getElementById("grid10").textContent = "Mobot - " + wsMsg.substr(12, 1);
            }
            if (wsMsg.substr(14, 1) != 0) {
                
                document.getElementById("grid11").style.backgroundColor = "darkcyan";
                document.getElementById("grid11").textContent = "Mobot - " + wsMsg.substr(14, 1);
            }
            if (wsMsg.substr(16, 1) != 0) {
                
                document.getElementById("grid12").style.backgroundColor = "darkcyan";
                document.getElementById("grid12").textContent = "Mobot - " + wsMsg.substr(16, 1);
            }
            if (wsMsg.substr(18, 1) != 0) {
                
                document.getElementById("grid13").style.backgroundColor = "darkcyan";
                document.getElementById("grid13").textContent = "Mobot - " + wsMsg.substr(18, 1);
            }

            if (wsMsg.substr(22, 1) != 0) {
                
                document.getElementById("grid20").style.backgroundColor = "darkcyan";
                document.getElementById("grid20").textContent = "Mobot - " + wsMsg.substr(22, 1);
            }
            if (wsMsg.substr(24, 1) != 0) {
                
                document.getElementById("grid21").style.backgroundColor = "darkcyan";
                document.getElementById("grid21").textContent = "Mobot - " + wsMsg.substr(24, 1);
            }
            if (wsMsg.substr(26, 1) != 0) {
                
                document.getElementById("grid22").style.backgroundColor = "darkcyan";
                document.getElementById("grid22").textContent = "Mobot - " + wsMsg.substr(26, 1);
            }
            if (wsMsg.substr(28, 1) != 0) {
                
                document.getElementById("grid23").style.backgroundColor = "darkcyan";
                document.getElementById("grid23").textContent = "Mobot - " + wsMsg.substr(28, 1);
            }
            if (wsMsg.substr(32, 1) != 0) {
                
                document.getElementById("grid30").style.backgroundColor = "darkcyan";
                document.getElementById("grid30").textContent = "Mobot - " + wsMsg.substr(32, 1);
            }
            if (wsMsg.substr(34, 1) != 0) {
                
                document.getElementById("grid31").style.backgroundColor = "darkcyan";
                document.getElementById("grid31").textContent = "Mobot - " + wsMsg.substr(34, 1);
            }
            if (wsMsg.substr(36, 1) != 0) {
                
                document.getElementById("grid32").style.backgroundColor = "darkcyan";
                document.getElementById("grid32").textContent = "Mobot - " + wsMsg.substr(36, 1);
            }
            if (wsMsg.substr(38, 1) != 0) {
                
                document.getElementById("grid33").style.backgroundColor = "darkcyan";
                document.getElementById("grid33").textContent = "Mobot - " + wsMsg.substr(38, 1);
            }

        };
    } catch (exception) {
        console.error(exception);
    }
}
/**
 * Send a message to the WebSocket server
 */
function onSendClick() {
    if (webSocket.readyState != WebSocket.OPEN) {
        console.error("webSocket is not open: " + webSocket.readyState);
        return;
    }
    var msg = document.getElementById("txtmessage").value;
    webSocket.send(msg);
    document.getElementById("txtmessage").value ="";
}

function clearinterface() {

    document.getElementById("grid00").style.backgroundColor = "white";
    document.getElementById("grid01").style.backgroundColor = "white";
    document.getElementById("grid02").style.backgroundColor = "white";
    document.getElementById("grid03").style.backgroundColor = "white";
    document.getElementById("grid10").style.backgroundColor = "white";
    document.getElementById("grid11").style.backgroundColor = "white";
    document.getElementById("grid12").style.backgroundColor = "white";
    document.getElementById("grid13").style.backgroundColor = "white";
    document.getElementById("grid20").style.backgroundColor = "white";
    document.getElementById("grid21").style.backgroundColor = "white";
    document.getElementById("grid22").style.backgroundColor = "white";
    document.getElementById("grid23").style.backgroundColor = "white";
    document.getElementById("grid30").style.backgroundColor = "white";
    document.getElementById("grid31").style.backgroundColor = "white";
    document.getElementById("grid32").style.backgroundColor = "white";
    document.getElementById("grid33").style.backgroundColor = "white";
    document.getElementById("grid00").textContent = "1";
    document.getElementById("grid01").textContent = "5";
    document.getElementById("grid02").textContent = "9";
    document.getElementById("grid03").textContent = "13";
    document.getElementById("grid10").textContent = "2";
    document.getElementById("grid11").textContent = "6";
    document.getElementById("grid12").textContent = "10";
    document.getElementById("grid13").textContent = "14";
    document.getElementById("grid20").textContent = "3";
    document.getElementById("grid21").textContent = "7";
    document.getElementById("grid22").textContent = "11";
    document.getElementById("grid23").textContent = "15";
    document.getElementById("grid30").textContent = "4";
    document.getElementById("grid31").textContent = "8";
    document.getElementById("grid32").textContent = "12";
    document.getElementById("grid33").textContent = "16";
}