#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_LSM303_U.h>

//Declare pin functions on Redboard
#define MS1 3
#define MS2 4
#define EN  2
//Motor1 pinout
#define M1Step 6
#define M1Dir  5
//Motor2 pinout
#define M2Step 10
#define M2Dir   9
#define delayvar 10
#define calibratedhead 135
#define fixpulse 2000
//Declare variables for functions
int x = 0;
//internal variables
int steps = 500;
int turnsteps = 500;
int currentheading = 180;
int targetheading;
const int trig = 13; // Trigger Pin of Ultrasonic Sensor
const int echo = 12; // Echo Pin of Ultrasonic Sensor
//functions inside
void calibrateheading();
void forward();
void reverse();
void left();
void right();
void checkultra();
Adafruit_LSM303_Mag_Unified mag = Adafruit_LSM303_Mag_Unified(12345); //magnetometer i2cid

void setup() {
  //GlobalPinouts for motor
  pinMode(MS1, OUTPUT);
  pinMode(MS2, OUTPUT);
  pinMode(EN, OUTPUT);
  //Motor1Pinouts
  pinMode(M1Step, OUTPUT);
  pinMode(M1Dir, OUTPUT);
  //motor2Pinouts
  pinMode(M2Step, OUTPUT);
  pinMode(M2Dir, OUTPUT);
  // ultrasonic
  pinMode(trig, OUTPUT);
  pinMode(echo, INPUT);
  //Wire.begin(8);                // join i2c bus with address #8
  //Wire.onReceive(receiveEvent); // register event
  Serial.begin(9600); //Open Serial connection for debugging
  if (!mag.begin())
  {
    /* There was a problem detecting the LSM303 ... check your connections */
    Serial.print("Ooops, no LSM303 detected ... Check your wiring!");
    while (1);
  }
  calibrateheading();
  Serial.println("Calibrated");
}
//void receiveEvent(int howMany) {
//  int readout = Wire.read();
//  Serial.println(readout);
//  if (readout == 4)
//  {
//    Serial.print("Now Forwarding");
//    forward();
//    Serial.print("DONE");
//  } else if (readout == 5)
//  {
//    left();
//  } else if (readout == 6)
//  {
//    right();
//  }
//}

void loop() {
  left();
  delay(2000);
  right();
  delay(2000);
  left();
  delay(2000);
  right();
  delay(2000);
  right();
  delay(2000);
  right();
  delay(2000);
  left();
  delay(2000);
  sensors_event_t event;
  mag.getEvent(&event);


}

int getheading()
{
  delay(1);
  x++;
  sensors_event_t event;
  mag.getEvent(&event);

  float Pi = 3.14159;

  // Calculate the angle of the vector y,x
  float heading = (atan2(event.magnetic.y, event.magnetic.x) * 180) / Pi;

  // Normalize to 0-360
  if (heading < 0)
  {
    heading = 360 + heading;
  }

  return heading;

}

void calibrateheading() {
  int del = 5;
  currentheading = getheading();
  Serial.println(currentheading);
  Serial.println(calibratedhead);
  digitalWrite(MS1, LOW);
  digitalWrite(MS2, LOW);
  digitalWrite(M1Dir, LOW);
  digitalWrite(M2Dir, HIGH);

  while (currentheading != calibratedhead)
  {
    if (currentheading >= calibratedhead - 10 && currentheading <= calibratedhead + 10 )
    {
      del = 50;
    } else {
      del = 5;
    }

    Serial.println(currentheading);
    Serial.println(calibratedhead);
    digitalWrite(M1Step, HIGH);
    digitalWrite(M2Step, HIGH);
    delay(del);
    digitalWrite(M1Step, LOW);
    digitalWrite(M2Step, LOW);
    delay(del);
    currentheading = getheading();
    Serial.println(currentheading);
    Serial.println(calibratedhead);
  }
  Serial.println("exited");
}
void forward()
{
  currentheading = getheading();
  digitalWrite(MS1, LOW);
  digitalWrite(MS2, LOW);
  digitalWrite(M1Dir, LOW); //Pull direction pin low to move "forward"
  digitalWrite(M2Dir, LOW); //Pull direction pin low to move "forward"

  for (int x = 1; x < fixpulse; x++) //Loop the forward stepping enough times for motion to be visible
  {
    checkultra();
    if (getheading() == currentheading) {
      digitalWrite(M1Step, HIGH);
      digitalWrite(M2Step, HIGH);
      delay(delayvar);
      digitalWrite(M1Step, LOW);
      digitalWrite(M2Step, LOW);
      delay(delayvar);
    } else if (getheading() > currentheading) {
      digitalWrite(M1Step, HIGH);
      digitalWrite(M2Step, LOW);
      delay(delayvar);
      digitalWrite(M1Step, LOW);
      digitalWrite(M2Step, LOW);
      delay(delayvar);
    } else if (getheading() < currentheading) {
      digitalWrite(M1Step, LOW);
      digitalWrite(M2Step, HIGH);
      delay(delayvar);
      digitalWrite(M1Step, LOW);
      digitalWrite(M2Step, LOW);
      delay(delayvar);
    }
  }
}

int convertleftheading() {
  if (getheading() + 90 > 360 )
  {
    targetheading = getheading() + 90 - 360;
    return targetheading;
  }
  else
  {
    if (getheading() + 90 == 360)
    {
      return 0;
    }
    return getheading() + 90;
  }
}

int convertrightheading() {
  if (getheading() - 90 < 0 )
  {
    targetheading = getheading() - 90 + 360;
    return targetheading;
  }
  else
  {
    if (getheading() - 90 == 360)
    {
      return 0;
    }
    Serial.println(getheading() - 90);
    return getheading() - 90;
  }
}
void left()
{
  int del = 5;
  currentheading = getheading();
  targetheading = convertleftheading();
  Serial.println(currentheading);
  Serial.println(targetheading);
  digitalWrite(MS1, LOW);
  digitalWrite(MS2, LOW);
  digitalWrite(M1Dir, LOW);
  digitalWrite(M2Dir, HIGH);

  while (currentheading != targetheading)
  {
    if (currentheading >= targetheading - 10 && currentheading <= targetheading + 10 )
    {
      del = 50;
    } else {
      del = 5;
    }

    Serial.println(currentheading);
    Serial.println(targetheading);
    digitalWrite(M1Step, HIGH);
    digitalWrite(M2Step, HIGH);
    delay(del);
    digitalWrite(M1Step, LOW);
    digitalWrite(M2Step, LOW);
    delay(del);
    currentheading = getheading();
  }
  Serial.println("exited");
}
void right()
{
  int del = 5;
  currentheading = getheading();
  targetheading = convertrightheading();
  Serial.println(currentheading);
  Serial.println(targetheading);
  digitalWrite(MS1, LOW);
  digitalWrite(MS2, LOW);
  digitalWrite(M1Dir, HIGH);
  digitalWrite(M2Dir, LOW);

  while (currentheading != targetheading)
  {
    if (currentheading >= targetheading - 10 && currentheading <= targetheading + 10 )
    {
      del = 50;
    } else {
      del = 5;
    }

    Serial.println(currentheading);
    Serial.println(targetheading);
    digitalWrite(M1Step, HIGH);
    digitalWrite(M2Step, HIGH);
    delay(del);
    digitalWrite(M1Step, LOW);
    digitalWrite(M2Step, LOW);
    delay(del);
    currentheading = getheading();
  }
  Serial.println("exited");
}

void checkultra()
{
  long duration, inches;
  digitalWrite(trig, LOW);
  delayMicroseconds(2);
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig, LOW);
  duration = pulseIn(echo, HIGH);
  inches = microsecondsToInches(duration);
  Serial.println(inches);
  if (inches < 10)
  {
    delay(200);
    checkultra();
  }
}

long microsecondsToInches(long microseconds) {
  return microseconds / 74 / 2;
}
