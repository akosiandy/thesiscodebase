#include <Wire.h>

//Declare pin functions on Redboard
#define MS1 3
#define MS2 4
#define EN  2
//Motor1 pinout
#define M1Step 6
#define M1Dir  5
//Motor2 pinout
#define M2Step 10
#define M2Dir   9
#define calibratedhead 125
#define delayvar 100
#define fixpulse 1500
#define turnpulse 125
unsigned long time_now = 0;
int period = 1000;
int currentheading;
int targetheading;
boolean getdata = true;
//Declare variables for functions
const int trig = 13; // Trigger Pin of Ultrasonic Sensor
const int echo = 12; // Echo Pin of Ultrasonic Sensor
//functions inside
void calibrateheading();
void forward();
void reverse();
void left();
void right();
void checkultra();

void setup() {
  //GlobalPinouts for motor
  pinMode(MS1, OUTPUT);
  pinMode(MS2, OUTPUT);
  pinMode(EN, OUTPUT);
  //Motor1Pinouts
  pinMode(M1Step, OUTPUT);
  pinMode(M1Dir, OUTPUT);
  //motor2Pinouts
  pinMode(M2Step, OUTPUT);
  pinMode(M2Dir, OUTPUT);
  // ultrasonic
  pinMode(trig, OUTPUT);
  pinMode(echo, INPUT);
  Wire.begin(8);                // join i2c bus with address #8
  Wire.onReceive(receiveEvent); // register event
  Serial.begin(9600); //Open Serial connection for debugging
  //calibrateheading();
  Serial.println("Calibrated");
}
void receiveEvent(int howMany) {
  int lowbyte = Wire.read();
  int highbyte = Wire.read();
  unsigned int readout = highbyte * 256 + lowbyte;
  if (readout == 400)
  {
    Serial.print("Now Forwarding");
    forward();
    Serial.print("DONE");
  } else if (readout == 500)
  {
    left();
    delay(200);
  } else if (readout == 600)
  {
    right();
    delay(200);
  } else {
    setheading(readout);
  }
}

void loop() {
  if (millis() > time_now + period) {
    time_now = millis();
    digitalWrite(EN, HIGH);
  }


}
int setheading(int curheading) {
  currentheading = curheading;
}
int getheading()
{
  return currentheading;

}

void calibrateheading() {
  int del = 5;
  currentheading = getheading();
  Serial.println(currentheading);
  Serial.println(calibratedhead);
  digitalWrite(MS1, LOW);
  digitalWrite(MS2, LOW);
  digitalWrite(M1Dir, LOW);
  digitalWrite(M2Dir, HIGH);

  while (currentheading != calibratedhead)
  {
    if (currentheading >= calibratedhead - 10 && currentheading <= calibratedhead + 10 )
    {
      del = 50;
    } else {
      del = 5;
    }
    digitalWrite(M1Step, HIGH);
    digitalWrite(M2Step, HIGH);
    delay(del);
    digitalWrite(M1Step, LOW);
    digitalWrite(M2Step, LOW);
    delay(del);
    currentheading = getheading();
    Serial.println(currentheading);
    Serial.println(calibratedhead);
  }
  Serial.println("exited");
}
void forward()
{
  currentheading = getheading();
  digitalWrite(MS1, HIGH);
  digitalWrite(MS2, LOW);
  digitalWrite(M1Dir, HIGH); //Pull direction pin low to move "forward"
  digitalWrite(M2Dir, HIGH); //Pull direction pin low to move "forward"
  digitalWrite(EN, LOW);
  int checkpulse = 10;
  for (int x = 1; x < fixpulse; x++) //Loop the forward stepping enough times for motion to be visible
  {
    //    if (checkpulse == x) {
    //      checkultra();
    //      checkpulse += 10;
    //    }
    if (getheading() == currentheading) {
      digitalWrite(M1Step, HIGH);
      digitalWrite(M2Step, HIGH);
      delay(delayvar);
      digitalWrite(M1Step, LOW);
      digitalWrite(M2Step, LOW);
      delay(delayvar);
    } else if (getheading() > currentheading) {
      digitalWrite(M1Step, HIGH);
      digitalWrite(M2Step, LOW);
      delay(delayvar);
      digitalWrite(M1Step, LOW);
      digitalWrite(M2Step, LOW);
      delay(delayvar);
    } else if (getheading() < currentheading) {
      digitalWrite(M1Step, LOW);
      digitalWrite(M2Step, HIGH);
      delay(delayvar);
      digitalWrite(M1Step, LOW);
      digitalWrite(M2Step, LOW);
      delay(delayvar);
    }
  }

}

int convertleftheading() {
  if (getheading() + 90 > 360 )
  {
    targetheading = getheading() + 90 - 360;
    return targetheading;
  }
  else
  {
    if (getheading() + 90 == 360)
    {
      return 0;
    }
    return getheading() + 90;
  }
}

int convertrightheading() {
  if (getheading() - 90 < 0 )
  {
    targetheading = getheading() - 90 + 360;
    return targetheading;
  }
  else
  {
    if (getheading() - 90 == 360)
    {
      return 0;
    }
    Serial.println(getheading() - 90);
    return getheading() - 90;
  }
}
void left()
{
  int del = 5;
  currentheading = getheading();
  targetheading = convertleftheading();
  Serial.println(currentheading);
  Serial.println(targetheading);
  digitalWrite(MS1, LOW);
  digitalWrite(MS2, LOW);
  digitalWrite(M1Dir, LOW);
  digitalWrite(M2Dir, HIGH);
  digitalWrite(EN, LOW);

  //  while (currentheading != targetheading)
  //  {
  //    if (currentheading >= targetheading - 10 && currentheading <= targetheading + 10 )
  //    {
  //      del = 50;
  //    } else {
  //      del = 5;
  //    }
  for (int x = 0 ; x <= turnpulse; x++) {
    Serial.println(currentheading);
    Serial.println(targetheading);
    digitalWrite(M1Step, HIGH);
    digitalWrite(M2Step, HIGH);
    delay(del);
    digitalWrite(M1Step, LOW);
    digitalWrite(M2Step, LOW);
    delay(del);
    //currentheading = getheading();
  }
}
void right()
{
  int del = 5;
  currentheading = getheading();
  targetheading = convertrightheading();
  Serial.println(currentheading);
  Serial.println(targetheading);
  digitalWrite(MS1, LOW);
  digitalWrite(MS2, LOW);
  digitalWrite(M1Dir, HIGH);
  digitalWrite(M2Dir, LOW);
  digitalWrite(EN, LOW);

  //  while (currentheading != targetheading)
  //  {
  //    if (currentheading >= targetheading - 10 && currentheading <= targetheading + 10 )
  //    {
  //      del = 50;
  //    } else {
  //      del = 5;
  //    }
  for (int x = 0 ; x <= turnpulse; x++) {
    Serial.println(currentheading);
    Serial.println(targetheading);
    digitalWrite(M1Step, HIGH);
    digitalWrite(M2Step, HIGH);
    delay(del);
    digitalWrite(M1Step, LOW);
    digitalWrite(M2Step, LOW);
    delay(del);
    currentheading = getheading();
  }
  Serial.println("exited");
}

void checkultra()
{
  long duration, inches;
  digitalWrite(trig, LOW);
  delayMicroseconds(2);
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig, LOW);
  duration = pulseIn(echo, HIGH);
  inches = microsecondsToInches(duration);
  Serial.println(inches);
  if (inches < 10)
  {
    delay(200);
    checkultra();
  }
}

long microsecondsToInches(long microseconds) {
  return microseconds / 74 / 2;
}
