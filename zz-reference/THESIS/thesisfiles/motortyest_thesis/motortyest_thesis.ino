//Declare pin functions on Redboard
#define MS1 3
#define MS2 4
#define EN  2
//Motor1 pinout
#define M1Step 6
#define M1Dir  5
//Motor2 pinout
#define M2Step 10 
#define M2Dir   9
//Declare variables for functions


void setup() {
  //GlobalPinouts for motor
  pinMode(MS1, OUTPUT);
  pinMode(MS2, OUTPUT);
  pinMode(EN, OUTPUT);
  //Motor1Pinouts
  pinMode(M1Step, OUTPUT);
  pinMode(M1Dir, OUTPUT);
  //motor2Pinouts
  pinMode(M2Step, OUTPUT);
  pinMode(M2Dir, OUTPUT);
  Serial.begin(9600); //Open Serial connection for debugging
}


void loop()
{
  digitalWrite(MS1, LOW);
  digitalWrite(MS2, LOW);
  Serial.println("Moving forward at default step mode.");
  digitalWrite(M1Dir, LOW); //Pull direction pin low to move "forward"
  digitalWrite(M2Dir, LOW); //Pull direction pin low to move "forward"
  for (int x = 1; x < 1000; x++) //Loop the forward stepping enough times for motion to be visible
  {
    digitalWrite(M1Step, HIGH); //Trigger one step forward
    digitalWrite(M2Step, HIGH); //Trigger one step forward
    delay(1);
    digitalWrite(M1Step, LOW); //Pull step pin low so it can be triggered again
     digitalWrite(M2Step, LOW); //Pull step pin low so it can be triggered again
    delay(1);
  }

  Serial.println("Moving in reverse at default step mode.");
   digitalWrite(M1Dir, HIGH); //Pull direction pin low to move "forward"
  digitalWrite(M2Dir, HIGH); //Pull direction pin low to move "forward"
  for (int x = 1; x < 1000; x++) //Loop the stepping enough times for motion to be visible
  {
    digitalWrite(M1Step, HIGH); //Trigger one step forward
    digitalWrite(M2Step, HIGH); //Trigger one step forward
    delay(1);
    digitalWrite(M1Step, LOW); //Pull step pin low so it can be triggered again
     digitalWrite(M2Step, LOW); //Pull step pin low so it can be triggered again
    delay(1);
  }

}
