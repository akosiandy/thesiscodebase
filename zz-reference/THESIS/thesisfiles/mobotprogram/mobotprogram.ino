#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <WebSocketsClient.h>
#include <Hash.h>
ESP8266WiFiMulti WiFiMulti;
WebSocketsClient webSocket;
String const Mobotname = "Mobot-TWO";
void webSocketEvent(WStype_t type, uint8_t * payload, size_t length) {

	switch(type) {
		case WStype_DISCONNECTED:
			Serial.println("["+Mobotname+"] Disconnected!\n");
			break;
		case WStype_CONNECTED: {
			Serial.println("["+Mobotname+"]" + "Connected to Server\n");

			// send message to server when Connected
			webSocket.sendTXT("["+Mobotname+"]" + " Succesfully Connected");
		}
			break;
		case WStype_TEXT://recieve data here
			Serial.printf("[Mobot1] get text: %s\n",payload);

			// send message to server reply after receving
			//webSocket.sendTXT("message here");
			break;
		case WStype_BIN:
			Serial.printf("[Mobot1] get binary length: %u\n", length);
			hexdump(payload, length);

			// send data to server
			// webSocket.sendBIN(payload, length);
			break;
	}

}

void setup() {
	Serial.begin(115200);

	//Serial.setDebugOutput(true);
	Serial.setDebugOutput(true);

	Serial.println();
	Serial.println();
	Serial.println();

	for(uint8_t t = 4; t > 0; t--) {
		Serial.printf("[SETUP] BOOT WAIT %d...\n", t);
		Serial.flush();
		delay(1000);
	}

	WiFiMulti.addAP("NXTSEMCORP","projectlkcs");
  Serial.println("connecting");
	//WiFi.disconnect();
	while(WiFiMulti.run() != WL_CONNECTED) {
		delay(1000);
	}

	// server address, port and URL
	webSocket.begin("10.0.0.185", 3000, "/");
  Serial.println("brgin");
	// event handler
	webSocket.onEvent(webSocketEvent);
  Serial.println("event");

	// try ever 5000 again if connection has failed
	webSocket.setReconnectInterval(5000);

}
  int x;
void loop() {
	if (Serial.available()>0)
 {
    String serialmessage = Serial.readString();
    senddata(serialmessage);
 }

 
  webSocket.sendTXT("["+Mobotname+"] " + x);
  x++;
  delay(2000);
 
}

void senddata(String message)
{
  webSocket.sendTXT("["+Mobotname+"] " + message);
}
