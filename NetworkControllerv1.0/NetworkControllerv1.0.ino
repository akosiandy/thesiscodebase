#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <Wire.h>
#include <WebSocketsClient.h>
#include <Hash.h>
#include <Arduino.h>
const char* ssid = "NXTSEMCORP";
const char* password = "projectlkcs";

ESP8266WiFiMulti WiFiMulti;
WebSocketsClient webSocket;
String const Mobotname = "Mobot-3";

void webSocketEvent(WStype_t type, uint8_t * payload, size_t length) {

  switch (type) {
    case WStype_DISCONNECTED:
      //Serial.println("[" + Mobotname + "] Disconnected!\n");
      break;
    case WStype_CONNECTED: {
        //Serial.println("[" + Mobotname + "]" + "Connected to Server\n");

        // send message to server when Connected
        webSocket.sendTXT("[" + Mobotname + "]" + " Succesfully Connected");
      }
      break;
    case WStype_TEXT://recieve data here

      Serial.printf("[Mobot1] get text: %s\n", payload);
      if (strncmp((const char*)payload, "forward", 6) == 0) {
        senddata("i received forward");
        //        Wire.beginTransmission(8); // transmit to device #8
        //        Wire.write(4);        // sends five bytes
        //        Wire.endTransmission();    // stop transmitting
      } else if (strncmp((const char*)payload, "left", 4) == 0) {
        senddata("i received LEFT");
        //        Wire.beginTransmission(8); // transmit to device #8
        //        Wire.write(5);        // sends five bytes
        //        Wire.endTransmission();    // stop transmitting
      } else if (strncmp((const char*)payload, "right", 5) == 0) {
        senddata("i received RIGHt");
        //        Wire.beginTransmission(8); // transmit to device #8
        //        Wire.write(6);        // sends five bytes
        //        Wire.endTransmission();    // stop transmitting
      }

      // stop transmitting
      // send message to server reply after receving
      //webSocket.sendTXT("message here");
      break;
  }

}

void setup() {
  Serial.begin(9600);           // start serial for output
  //Wire.begin();

  for (uint8_t t = 4; t > 0; t--) {
    delay(1000);
  }

  WiFiMulti.addAP(ssid, password);

  //WiFi.disconnect();
  while (WiFiMulti.run() != WL_CONNECTED) {
    delay(1000);
  }

  // server address, port and URL
  webSocket.begin("10.0.0.192", 3000, "/");

  // event handler
  webSocket.onEvent(webSocketEvent);


  // try ever 5000 again if connection has failed
  webSocket.setReconnectInterval(5000);

  ArduinoOTA.setHostname("NRM-M1");
  //ArduinoOTA.setPassword("admin");
  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { // U_SPIFFS
      type = "filesystem";
    }

    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.print("Error");
  });
  ArduinoOTA.begin();
}
int x;
void loop() {
  ArduinoOTA.handle();
  if (Serial.available() > 0)
  {
    //String readout = Serial.readString();
    //Serial.print(readout);
  }
  delay(100);
}


void senddata(String message)
{
  webSocket.sendTXT("[" + Mobotname + "] " + message);
}
